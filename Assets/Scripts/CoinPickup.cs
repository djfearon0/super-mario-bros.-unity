﻿using UnityEngine;
using System.Collections;

public class CoinPickup : MonoBehaviour {

     public GameObject coin;

     void Start()
     {
          coin = GetComponent<GameObject>();
     }

	void OnCollisionEnter2D(Collision2D other)
     {
          if(other.gameObject.tag == "Player")
          {
               Destroy(coin);
          }
     }
}
