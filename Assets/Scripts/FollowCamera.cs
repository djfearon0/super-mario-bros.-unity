﻿using UnityEngine;
using System.Collections;

public class FollowCamera : MonoBehaviour {

     public Transform target;
     private Vector3 vel = Vector3.zero;
     private float damp = 0.15f;
     public GUIStyle style;

	// Use this for initialization
	void Start () {
          style.fontSize = 30;
          style.normal.textColor = Color.white;
	}
	
	// Update is called once per frame
	void Update () {
          if(target)
          {
               Vector3 point = GetComponent<Camera>().WorldToViewportPoint(target.position);
               Vector3 delta = target.position - GetComponent<Camera>().ViewportToWorldPoint(new Vector3(0.5f, 0.4f, point.z));
               Vector3 destination = transform.position += delta;
               transform.position = Vector3.SmoothDamp(transform.position, destination, ref vel, damp);
          }
	}

     void OnGUI()
     {
          GUI.Label(new Rect(20, 5, 50, 30), "MARIO", style);
          GUI.Label(new Rect(Screen.width - 120, 5, 50, 30), "TIME", style);
     }
}
