﻿using UnityEngine;
using System.Collections;

public class Mario : MonoBehaviour {

     private string moveTrigger = "Move";
     private string jumpTrigger = "Jump";
     private string lostTrigger = "Lost";

     private int score;
     private int time;
     private float currentTime;
     private float moveSpeed = 100.0f;
     private float quitTime;
     private bool canJump;
     private bool lost;
     private bool win;
     private bool facingLeft;

     private Animator anim;
     private Rigidbody2D rb;
     private Collider2D collider;
     public GUIStyle myStyle;
     public GUIStyle winStyle;
     public AudioClip jump;
     public AudioClip coin;
     public AudioClip lowNote;
     private AudioSource source;

	// Use this for initialization
	void Start () {
          anim = GetComponent<Animator>();
          collider = GetComponent<Collider2D>();
          rb = GetComponent<Rigidbody2D>();
          source = GetComponent<AudioSource>();
          facingLeft = false;
          anim.SetBool(lostTrigger, false);
          lost = false;
          win = false;
          canJump = true;
          score = 0;
          myStyle.fontSize = 30;
          myStyle.normal.textColor = Color.white;
          winStyle.fontSize = 50;
          winStyle.normal.textColor = Color.green;
          time = 200;
	}
	
	// Update is called once per frame
	void Update () {

          if(lost == true)
          {
               source.clip = lowNote;
               source.Play();
               anim.SetBool(lostTrigger, true);
               canJump = false;
               lost = true;
               Credits();
          }

          if(transform.position.y < -15)
          {
               lost = true;
          }

          if(win == true)
          {
               moveSpeed = 0;
               Invoke("Credits", 3.0f);
          }

          if (lost == false)
          {
               currentTime = Time.time;
          }
          else
          {
               currentTime = time;
          }
          

          OnGUI();
          
          float move = Input.GetAxis("Horizontal");

          if(lost == true)
          {
               move = 0;
          }

          if (move > 0)
          {
               anim.SetTrigger(moveTrigger);
               if(facingLeft == true)
               {
                    transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
                    facingLeft = false;
               }
               transform.position += new Vector3 (0.1f * Time.deltaTime * moveSpeed, 0f, 0f);

          }

          if (move < 0)
          {
               anim.SetTrigger(moveTrigger);
               if(facingLeft == false)
               {
                    transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
                    facingLeft = true;
               }
               transform.position += new Vector3 (-0.1f * Time.deltaTime * moveSpeed, 0f, 0f);
          }

          if (Input.GetKeyDown(KeyCode.W))
          {
               if(canJump)
               {
                    canJump = false;
                    anim.SetBool(jumpTrigger, true);
                    moveSpeed = 75f;
                    source.clip = jump;
                    source.Play();
                    rb.velocity = new Vector2(0f, 15f);
               }
          }

          if((time-((int)currentTime)) == 0)
          {
               anim.SetBool(lostTrigger, true);
               canJump = false;
               lost = true;
               collider.isTrigger = true;
          }
          
	}

     void OnCollisionEnter2D(Collision2D other)
     {
          if (other.gameObject.tag == "Ground" || other.gameObject.tag == "Pipe")
          {
               anim.SetBool(jumpTrigger, false);
               canJump = true;
               moveSpeed = 100f;
          }

          if (other.gameObject.tag == "Castle" && win == false)
          {
               score += ((time-((int)currentTime)) * 500);
               win = true;
          }

          if (other.gameObject.tag == "Coin")
          {
               score += 200;
               source.clip = coin;
               source.Play();
               Destroy(other.gameObject);
          }

          if (other.gameObject.tag == "Piranha")
          {
               rb.velocity = new Vector2(0f, 20f);
               anim.SetBool(lostTrigger, true);
               canJump = false;
               lost = true;
               collider.isTrigger = true;
          }

          if ((other.gameObject.tag == "Enemy") && ((transform.position.x > other.collider.bounds.center.x + 0.9) || (transform.position.x < other.collider.bounds.center.x - 0.9)))
          {
               rb.velocity = new Vector2(0f, 20f);
               anim.SetBool(lostTrigger, true);
               canJump = false;
               lost = true;
               collider.isTrigger = true;
          }

          if (other.gameObject.tag == "Enemy" && ((transform.position.y > other.collider.bounds.center.y + 0.4)))
          {
               rb.velocity = new Vector2(0f, 10f);
               anim.SetBool(jumpTrigger, true);
               score += 100;
          }
     }

     void OnGUI()
     {
          GUI.Label(new Rect(20, 40, 50, 30), score + "", myStyle);
          GUI.Label(new Rect(Screen.width - 100, 40, 50, 30), (time - ((int)currentTime)) + "", myStyle);
          if(win == true)
          {
               GUI.Label(new Rect(Screen.width/2 - 200, 50, 50, 30), "Congratulations!", winStyle);
          }
     }

     void Credits()
     {
          Application.LoadLevel("Credits");
     }
}
