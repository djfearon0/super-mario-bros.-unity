﻿using UnityEngine;
using System.Collections;

public class Goomba : MonoBehaviour {

     private float moveSpeed = 30.0f;
     private Collider2D collider;
     public AudioClip hit;
     private AudioSource source;

	// Use this for initialization
	void Start () {
          collider = GetComponent<Collider2D>();
          source = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
          transform.position += new Vector3(-0.1f * Time.deltaTime * moveSpeed, 0f, 0f);
	}

     void OnCollisionEnter2D(Collision2D other)
     {

          if (other.gameObject.tag == "Pipe")
          {
               moveSpeed *= -1;
          }
          if (other.gameObject.tag == "Player" && (transform.position.y + 0.2 < other.collider.bounds.center.y - 0.3))
          {
               source.clip = hit;
               source.Play();
               collider.isTrigger = true;
          }
     }
}
