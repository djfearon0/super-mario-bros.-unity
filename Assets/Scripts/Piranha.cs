﻿using UnityEngine;
using System.Collections;

public class Piranha : MonoBehaviour {

     public float maxHeight;
     public float minHeight;
     private bool up;
     private float delay;
     private float nextUsage;

	// Use this for initialization
	void Start () {
          Time.fixedDeltaTime = 0.0001f;
          up = false;
          delay = 4;
          nextUsage = 0f;
     }
	
	// Update is called once per frame
	void FixedUpdate () {
          if (up == false && Time.time > nextUsage)
          {
               if (transform.position.y < maxHeight)
               {
                    transform.Translate(Vector3.up * 1f * Time.deltaTime);
               }
               if (transform.position.y > maxHeight-1)
               {
                    up = true;
                    nextUsage = Time.time + delay;
               }
          }

          else if (up == true && Time.time > nextUsage)
          {
               if (transform.position.y >= minHeight)
               {
                    transform.Translate(Vector3.down * 1f * Time.deltaTime);
               }
               if (transform.position.y == minHeight)
               {
                    up = false;
                    nextUsage = Time.time + delay;
               }
          }
	}

     void wait()
     {
          
     }
}
